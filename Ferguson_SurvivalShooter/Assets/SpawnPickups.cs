﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPickups : MonoBehaviour
{
    //array of possible pickups
    public GameObject[] pickupPrefabs;
    //reference to pickup i spawned
    GameObject spawnedPickup;
    float waitToSpawn = 0f;

    // Start is called before the first frame update
    void Start()
    {
        SpawnPickup();
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnedPickup == null)
        {
            if (waitToSpawn <= 0f)
                SpawnPickup();
            else
                waitToSpawn -= Time.deltaTime;
        }
    }

    void SpawnPickup()
    {
        //pick random object to spawn from array
        GameObject toSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        spawnedPickup = Instantiate(toSpawn,transform.position, toSpawn.transform.rotation) as GameObject;
        waitToSpawn = Random.Range(8f, 15f);
    }
}
