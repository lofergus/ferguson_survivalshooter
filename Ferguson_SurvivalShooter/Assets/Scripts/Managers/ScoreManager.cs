﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public Text[] highScores;
    //PlayerHealth playerHealth;

    Text text;
    int[] highScoreValues;

    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
        //playerHealth = GetComponent<PlayerHealth>();

    }

    private void Start()
    {
        //establishes high score value ints for high score
        highScoreValues = new int[highScores.Length];
        //loop through each high score in array using length of high score array
        for (int x = 0; x < highScores.Length; x++)
        {
            //populate high score values
            highScoreValues[x] = PlayerPrefs.GetInt("highScoreValues" + x);
        }
        //call draw score
        DrawScores();
    }
    //function saves scores to high score values
    void SaveScores()
    {
        //loop through each item in high scores using the length of the array
        for (int x = 0; x < highScores.Length; x++)
        {
            PlayerPrefs.SetInt("highScoreValues" + x,highScoreValues[x]);
        }
    }
    public void CheckForHighScore(int _value)
    {
        //loop amount of times for however many assigned high scores
        for(int x=0; x < highScores.Length; x++)
        {
            //check each value and compare it to values of high scoers
            if (_value > highScoreValues[x])
            {
                //loop through remaining items in high scores to check 
                //if high score is lower or higher and assign it to appropriate 
                //spot on leaderboard
                for (int y = highScores.Length - 1; y > x; y--)
                {
                    //place score in appropriate leaderboard spot
                    highScoreValues[y] = highScoreValues[y - 1];
                }
                highScoreValues[x] = _value;
                //call draw score function which outputs to canvas
                DrawScores();
                //save scores using playerprefs
                SaveScores();
                break;
            }
        }
    }
    //function outputs scores to canvas
    void DrawScores()
    {
        //loop through all high scores
        for (int x = 0; x < highScores.Length; x++)
        {
            //for each item in highscores, convert it to string
            highScores[x].text = highScoreValues[x].ToString();
        }
    }
    void Update ()
    {
        //sets score text on screen
        text.text = "Score: " + score;
        
    }
}
