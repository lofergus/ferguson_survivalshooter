﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    public Text bulletCount;
    //public Button restart;
    //public Button exit;

    public GameObject healthBurst;
    public GameObject ammoBurst;

    float deathTimer;
    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    public ScoreManager scoreManager;
    bool isDead;
    bool damaged;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;

        bulletCount.text = "Bullets Remaining: " + playerShooting.startingBullets + "/120";
    }


    public void TakeDamage (int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        {
            Death ();
            Invoke("ReturnMainMenu", 10);
        }
    }


    void Death ()
    {
        isDead = true;
        //deathTimer += Time.deltaTime;
        playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        playerShooting.enabled = false;

        scoreManager.CheckForHighScore(ScoreManager.score);
        //restart.interactable = true;
        //exit.interactable = true;

        
    }

    void ReturnMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }


    public void RestartLevel ()
    {
        print("hello");
        SceneManager.LoadScene (0);
    }
    //The following code allows for the pickups to work for various health levels
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Health"))
        {
            Destroy(other.gameObject);
            Instantiate(healthBurst, other.transform.position, healthBurst.transform.rotation);
            //checks to see if the player can take an additional 25 health, if they can, give them 25 once picked up
            if (currentHealth <= 75)
            {
                currentHealth += 25;
                healthSlider.value = currentHealth;
                
            }
            //if the player cannot take the full amount of health from a pickup
            //they will be filled to max, not filling them past 100 health
            if (currentHealth > 75 && currentHealth < 100)
            {
                currentHealth = 100;
                healthSlider.value = currentHealth;
            }
        }
        //if the pickup is designated as ammunition
        if (other.gameObject.CompareTag("Ammo"))
        {
            Instantiate(ammoBurst, other.transform.position, ammoBurst.transform.rotation);
            //first remove the object so it no longer exists
            Destroy(other.gameObject);
            //if the player has less than or equal to 30 bullets, add 30 bullets
            if (playerShooting.startingBullets <= 60)
            {
                playerShooting.startingBullets += 60;

            }
            //if they have between 31-60 bullets, set their ammo to 60/60 to avoid overfilling ammunition
            //if (playerShooting.startingBullets >30 && playerShooting.startingBullets <= 60)
            else
            {
                playerShooting.startingBullets = 120;
            }
        }


    }
}
