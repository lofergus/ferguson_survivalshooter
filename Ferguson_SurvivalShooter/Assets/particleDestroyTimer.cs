﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleDestroyTimer : MonoBehaviour
{
    public float timeTilDestroy;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyMe", timeTilDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        void DestroyMe()
        {
            Destroy(gameObject);
        }
    }
}
